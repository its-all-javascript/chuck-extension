import React, { useState, useEffect } from 'react';
import './App.css';

const App = () => {
  /* factoid adında yeni bir state değişkeni ve setter'ı tanımlıyoruz */
  const [factoid, setFactoid] = useState({});

  
  const fetchFactoid = () =>  {
    fetch("https://api.chucknorris.io/jokes/random")
      .then(res => res.json())
      .then(setFactoid)
  }

  useEffect(fetchFactoid, [])

  return (
    <div className="App">
      { factoid.value && 
        <div className="factoid">{factoid.value}</div>
      }
    </div>
  );
}

export default App;
